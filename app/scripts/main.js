$(document).ready(function () {

  $.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
  };

  $('.iv-view__title_fixed').css({
    marginTop: ($('.iv-top').outerHeight() + $('.iv-header').outerHeight())
  });

  $(window).bind('load resize scroll', function () {

    if ($('.iv-view').isInViewport()) {
      $('.iv-view__title_fixed').slideUp('fast', function () {
        // Animation complete.
      });
    } else {
      $('.iv-view__title_fixed').slideDown('fast', function () {
        // Animation complete.
      });
    };

    if ($(window).width() > 1199) {

      if ($('.col-md-3').offset().top - $(window).scrollTop() < 0) {
        $('.iv-aside_fixed').css({
          marginTop: ($('.iv-top').outerHeight() + $('.iv-header').outerHeight() + $('.iv-view__title_fixed').outerHeight() + 20)
        });
        $('.iv-aside').addClass('iv-aside_fixed');
      } else {
        $('.iv-aside_fixed').css({
          marginTop: 0
        });
        $('.iv-aside').removeClass('iv-aside_fixed');
      };

    } else {
      $('.iv-aside_fixed').css({
        marginTop: 0
      });
      $('.iv-aside').removeClass('iv-aside_fixed');
    };

    if ($('.iv-bottom').isInViewport()) {
      $('.iv-aside').removeClass('iv-aside_fixed');
    };

    if ($(window).width() < 992) {

      $('.iv-view__title_fixed').css({
        marginTop: 0
      });

    };

  });

  $('.iv-welcome').slick({
    arrows: true,
    dots: false,
    autoplay: true
  });

  $('.iv-reviews__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    speed: 300
  });

  $('.iv-companies__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    speed: 10000,
    autoplaySpeed: 0,
    cssEase: 'linear',
    easing: 'linear',
    slidesToShow: 8,
    slidesToScroll: 8,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.iv-decision__slides').slick({
    arrows: true,
    dots: false,
    autoplay: true
  });

  $('.iv-papers__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.iv-things__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.iv-about__slick').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 2,
    slidesToScroll: 2,
    centerMode: false,
    centerPadding: '0px',
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.iv-projects').slick({
    arrows: true,
    dots: false,
    autoplay: true
  });

  $('.owl-carousel').owlCarousel({
    items: 1,
    loop: false,
    URLhashListener: true,
    autoplayHoverPause: true,
    startPosition: 'URLHash'
  });

  $('.iv-header__droper').click(function () {
    $('.iv-header__nav').slideToggle('fast', function () {
      // Animation complete.
    });
  });

});

